appRoutes.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');

    $stateProvider
        .state('home', {
            url: '/home',
            controller: 'HomeCtrl',
            templateUrl: 'views/home.html'
        })
        //.state('landing@home', {
        //    templateUrl: 'views/home/landing.html'
        //})
        .state('app', {
            url: '/app/:uid',
            abstract: true,
            controller: 'AppCtrl',
            templateUrl: 'views/partials/app.html'
        })
        .state('app.dashboard', {
            url: '/dashboard',
            controller: 'DashboardCtrl',
            templateUrl: 'views/partials/app.dashboard.html'
        })
        .state('app.customers', {
            url: '/customers',
            controller: 'CustomersCtrl',
            templateUrl: 'views/partials/app.customers.html'
        })
        .state('login', {
            url: '/login',
            controller: 'LoginCtrl',
            templateUrl: 'views/login.html'
        })
});