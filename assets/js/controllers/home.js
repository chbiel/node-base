/**
 * @author Christopher Biel <christopher.biel89@gmail.com>
 * @package Nexployer_Frontend
 * @version 27.03.2015 12:51
 */
appControllers.controller('HomeCtrl', ['$scope', '$http',
    function HomeCtrl($scope, $http) {

        //$scope.myInterval = 5000;
        //var slides = $scope.slides = [];
        //$scope.addSlide = function() {
        //	var newWidth = 600 + slides.length + 1;
        //	slides.push({
        //		image: 'http://placekitten.com/' + newWidth + '/300',
        //		text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' ' +
        //		['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
        //	});
        //};
        //for (var i=0; i<4; i++) {
        //	$scope.addSlide();
        //}

        $scope.welcome = "Landing page for App!";
    }
]);

appControllers.controller('HomeTopCarouselCtrl', ['$scope',
    function HomeTopCarouselCtrl($scope) {
        $scope.myInterval = 3000;
        $scope.slides = [
            {
                image: '//placehold.it/1024x700/449955/FFF'
            },
            {
                image: '//placehold.it/1024x700'
            },
            {
                image: '//placehold.it/1024x700/CC1111/FFF'
            }
        ];
    }
]);

appControllers.controller('HomeLoginModalCtrl', ['$scope',
    function HomeLoginModalCtrl($scope) {
        $scope.myInterval = 3000;
        $scope.slides = [
            {
                image: '//placehold.it/1024x700/449955/FFF'
            },
            {
                image: '//placehold.it/1024x700'
            },
            {
                image: '//placehold.it/1024x700/CC1111/FFF'
            }
        ];
    }
]);