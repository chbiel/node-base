/**
 * @author Christopher Biel <christopher.biel89@gmail.com>
 * @package ShipUp
 * @version 14.03.2015 15:03
 */
var baseUrlPath = '/user';

appServices.factory('UserService', function($http, $q, Options) {
    return {
        read: function(id) {
            var deferred = $q.defer();

            $http.get(Options.baseUrl + baseUrlPath + '/read/' + id).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        create: function(data) {
            var deferred = $q.defer();

            $http.post(Options.baseUrl + baseUrlPath + '/create', {data: "my data"}).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        edit: function(data) {
            var deferred = $q.defer();

            $http.put(Options.baseUrl + baseUrlPath + '/edit', {data: "my data"}).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        delete: function(id) {
            var deferred = $q.defer();

            $http.delete(Options.baseUrl + baseUrlPath + '/delete/' + id).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        }
    };
});