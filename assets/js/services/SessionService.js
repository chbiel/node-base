/**
 * @author Christopher Biel <christopher.biel89@gmail.com>
 * @package ShipUp
 * @version 14.03.2015 16:15
 */
var baseUrlPath = '/session';

appServices.factory('SessionService', function ($http, $q, localStorageService, Options) {


    return {
        isActive: function (id) {
            var deferred = $q.defer();

            if (localStorageService.get(TOKEN_KEY)) {

            } else {

            }

            return deferred.promise;
        },


        read: function (id) {
            var deferred = $q.defer();

            $http.get(Options.baseUrl + baseUrlPath + '/read/' + id).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        create: function (data) {
            var deferred = $q.defer();

            $http.post(Options.baseUrl + baseUrlPath + '/create', {
                data: {
                    username: data.username,
                    password: data.password
                }
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        edit: function (data) {
            var deferred = $q.defer();

            $http.put(Options.baseUrl + baseUrlPath + '/edit', {data: "my data"}).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        delete: function (id) {
            var deferred = $q.defer();

            $http.delete(Options.baseUrl + baseUrlPath + '/delete/' + id).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        }
    };
});