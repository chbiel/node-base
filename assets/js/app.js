'use strict';

var app = angular.module('app', ['ui.router', 'ui.bootstrap', 'ui.utils', 'LocalStorageModule', 'appRoutes', 'appControllers', 'appServices', 'appDirectives', 'appFilters']);
var appRoutes = angular.module('appRoutes', []);
var appControllers = angular.module('appControllers', ['LocalStorageModule']);
var appServices = angular.module('appServices', ['LocalStorageModule']);
var appDirectives = angular.module('appDirectives', []);
var appFilters = angular.module('appFilters', []);

app.constant('Options', {
    baseUrl: 'http://localhost:3001',
    applicationName: 'ShipUp'
});

app
    .config(function (localStorageServiceProvider, Options) {
        localStorageServiceProvider
            .setPrefix(Options.applicationName)
            .setStorageType('sessionStorage')
            .setNotify(true, true);
    })
    .run(function($rootScope, $window) {
        /**
         * $rootScope.doingResolve is a flag useful to display a spinner on changing states.
         * Some states may require remote data so it will take awhile to load.
         */
        var resolveDone = function () { $rootScope.doingResolve = false; };
        $rootScope.doingResolve = false;

        $rootScope.$on('$stateChangeStart', function () {
            $rootScope.doingResolve = true;
        });
        $rootScope.$on('$stateChangeSuccess', resolveDone);
        $rootScope.$on('$stateChangeError', resolveDone);
        $rootScope.$on('$statePermissionError', resolveDone);
    });

