var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-ruby-sass');
var sourcemaps = require('gulp-sourcemaps');
var debug = require('gulp-debug');

var paths = {
	libraries: [
		'bower_components/jquery/dist/jquery.min.js',
		'bower_components/bootstrap-sass-official/javascript/bootstrap.js',
		'bower_components/angular/angular.js',
		'bower_components/angular-bootstrap/ui-bootstrap.min.js',
		'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
		'bower_components/angular-ui-router/release/angular-ui-router.min.js',
		'bower_components/angular-ui-utils/ui-utils.min.js',
		'bower_components/angular-ui-utils/ui-utils-ieshiv.min.js',
		'bower_components/angular-local-storage/dist/angular-local-storage.min.js',
		'bower_components/angular-acl/angular-acl.min.js'
	],
	userscripts: [
		'assets/js/app.js',
		'assets/js/routes/**/*.js',
		'assets/js/controllers/**/*.js',
		'assets/js/directives/**/*.js',
		'assets/js/filters/**/*.js',
		'assets/js/services/**/*.js'
	]
};

gulp.task('libraries', function() {
	return gulp.src(paths.libraries)
		.pipe(debug())
		.pipe(concat('libraries.js', {newLine: '\n'}))
		.pipe(gulp.dest('dist/js'));
});

gulp.task('userscripts', function() {
	return gulp.src(paths.userscripts)
		.pipe(debug())
		.pipe(concat('app.js'))
		.pipe(gulp.dest('dist/js'));
});

gulp.task('styles', function() {
	return sass('assets/css/sass/', {style: 'expanded', sourcemap: true, force: true})
		.pipe(sourcemaps.write())
		.pipe(debug())
		.pipe(gulp.dest('dist/css'));
});

gulp.task('watch', function() {
	gulp.watch(paths.libraries, ['libraries']);
	gulp.watch(paths.userscripts, ['userscripts']);
	gulp.watch('assets/css/sass/**', ['styles']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['libraries', 'userscripts', 'styles']);
