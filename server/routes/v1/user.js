/**
 * @author Christopher Biel <christopher.biel89@gmail.com>
 * @package ShipUp
 * @version 13.03.2015 21:01
 */
var passwordHash = require('password-hash');

var UserModel = require('../../models/userModel').UserModel;

exports.read = function(req, res) {
    var id = req.params.id;

    var query = UserModel.findOne({uid: id});
    query.exec(function(err, data) {
    	if (err) {
    		return res.send(404, err);
    	}

    	return res.send(200, {data: data});
    });

    return res.send(200, {data: "Hello world!"});
};

exports.create = function(req, res) {
    var data = req.body.data;

    var newPerson = new UserModel();
    newPerson.name =  {first: data.first, last: data.last};
    newPerson.email = data.email;
    newPerson.password = passwordHash.generate(data.password);

    newPerson.save(function(err) {
    	if (err) {
    		return res.send(404, err);
    	}
        return res.send(200);
    });

    return res.send(200);
};

exports.edit = function(req, res) {
    var data = req.body.data;

    UserModel.findOneAndUpdate({uid: data.uid}, {
        name: {first: data.first, last: data.last},
        email: data.email,
        gender: data.gender
    }, function (err, nbRows, raw) {
        if (err) {
            return res.send(400);
        }
        return res.send(200);
    });

    // MySchemaModel.update({uid: data.uid},
    // 	{field: data.field},
    //
    // );

    return res.send(200);
};

exports.delete = function(req, res) {
    var id = req.params.id;

    var query = UserModel.findOne({uid: id});
    query.exec(function(err, data) {
    	if (err) {
    		return res.send(404, err);
    	}
    	if (data != null) {
    		data.remove();
    	}
    	return res.send(200);
    });

    return res.send(200);
};