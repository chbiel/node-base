/**
 * @author Christopher Biel <christopher.biel89@gmail.com>
 * @package ShipUp
 * @version 13.03.2015 21:02
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var User = new Schema({
    uid: { type: Schema.Types.ObjectId, required: true },
    email: { type: String, required: true, index: { unique: true, sparse: true }, regex: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/ },
    password: { type: String, required: true },
    name: {
        first: String,
        last : String
    },
    gender: String,
    roles: { type: Array, required: true },
    created: { type: Date, default: Date.now }
});

exports.UserModel = mongoose.model('User', User);