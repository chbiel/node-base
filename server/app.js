var express = require('express');
var app = express();
var bodyParser = require('body-parser'); //bodyparser + json + urlencoder
var morgan  = require('morgan'); // logger
var db = require('./configs/database');

//Configuration
app.set('port', 3001);
app.listen(app.get('port'));
app.use(bodyParser());
app.use(morgan());

app.all('*', function(req, res, next) {
	res.set('Access-Control-Allow-Origin', 'http://localhost');
	res.set('Access-Control-Allow-Credentials', true);
	res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
	res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type');
	if ('OPTIONS' == req.method) return res.send(200);
	next();
});


//Routes
var routes = {};
routes.example = require('./routes/v1/example.js');
routes.user = require('./routes/v1/user.js');


app.get('/', routes.example.read);

//Routing URLs
app.get('/read/:id', routes.example.read);
app.post('/create', routes.example.create);
app.put('/edit', routes.example.edit);
app.delete('/delete/:id', routes.example.delete);

app.get('/v1/user/read/:id', routes.user.read);
app.post('/v1/user/create', routes.user.create);
app.put('/v1/user/edit/:id', routes.user.edit);
app.delete('/v1/user/delete/:id', routes.user.delete);



console.log('[INFO] Your project API started on port ' + app.get('port'));