'use strict';

var app = angular.module('app', ['ui.router', 'ui.bootstrap', 'ui.utils', 'LocalStorageModule', 'appRoutes', 'appControllers', 'appServices', 'appDirectives', 'appFilters']);
var appRoutes = angular.module('appRoutes', []);
var appControllers = angular.module('appControllers', ['LocalStorageModule']);
var appServices = angular.module('appServices', ['LocalStorageModule']);
var appDirectives = angular.module('appDirectives', []);
var appFilters = angular.module('appFilters', []);

app.constant('Options', {
    baseUrl: 'http://localhost:3001',
    applicationName: 'ShipUp'
});

app
    .config(function (localStorageServiceProvider, Options) {
        localStorageServiceProvider
            .setPrefix(Options.applicationName)
            .setStorageType('sessionStorage')
            .setNotify(true, true);
    })
    .run(function($rootScope, $window) {
        /**
         * $rootScope.doingResolve is a flag useful to display a spinner on changing states.
         * Some states may require remote data so it will take awhile to load.
         */
        var resolveDone = function () { $rootScope.doingResolve = false; };
        $rootScope.doingResolve = false;

        $rootScope.$on('$stateChangeStart', function () {
            $rootScope.doingResolve = true;
        });
        $rootScope.$on('$stateChangeSuccess', resolveDone);
        $rootScope.$on('$stateChangeError', resolveDone);
        $rootScope.$on('$statePermissionError', resolveDone);
    });


appRoutes.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');

    $stateProvider
        .state('home', {
            url: '/home',
            controller: 'HomeCtrl',
            templateUrl: 'views/home.html'
        })
        //.state('landing@home', {
        //    templateUrl: 'views/home/landing.html'
        //})
        .state('app', {
            url: '/app/:uid',
            abstract: true,
            controller: 'AppCtrl',
            templateUrl: 'views/partials/app.html'
        })
        .state('app.dashboard', {
            url: '/dashboard',
            controller: 'DashboardCtrl',
            templateUrl: 'views/partials/app.dashboard.html'
        })
        .state('app.customers', {
            url: '/customers',
            controller: 'CustomersCtrl',
            templateUrl: 'views/partials/app.customers.html'
        })
        .state('login', {
            url: '/login',
            controller: 'LoginCtrl',
            templateUrl: 'views/login.html'
        })
});
/**
 * @author Christopher Biel <christopher.biel89@gmail.com>
 * @package Nexployer_Frontend
 * @version 27.03.2015 12:51
 */
appControllers.controller('HomeCtrl', ['$scope', '$http',
    function HomeCtrl($scope, $http) {

        //$scope.myInterval = 5000;
        //var slides = $scope.slides = [];
        //$scope.addSlide = function() {
        //	var newWidth = 600 + slides.length + 1;
        //	slides.push({
        //		image: 'http://placekitten.com/' + newWidth + '/300',
        //		text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' ' +
        //		['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
        //	});
        //};
        //for (var i=0; i<4; i++) {
        //	$scope.addSlide();
        //}

        $scope.welcome = "Landing page for App!";
    }
]);

appControllers.controller('HomeTopCarouselCtrl', ['$scope',
    function HomeTopCarouselCtrl($scope) {
        $scope.myInterval = 3000;
        $scope.slides = [
            {
                image: '//placehold.it/1024x700/449955/FFF'
            },
            {
                image: '//placehold.it/1024x700'
            },
            {
                image: '//placehold.it/1024x700/CC1111/FFF'
            }
        ];
    }
]);

appControllers.controller('HomeLoginModalCtrl', ['$scope',
    function HomeLoginModalCtrl($scope) {
        $scope.myInterval = 3000;
        $scope.slides = [
            {
                image: '//placehold.it/1024x700/449955/FFF'
            },
            {
                image: '//placehold.it/1024x700'
            },
            {
                image: '//placehold.it/1024x700/CC1111/FFF'
            }
        ];
    }
]);
/**
 * @author Christopher Biel <christopher.biel89@gmail.com>
 * @package Nexployer_Frontend
 * @version 27.03.2015 12:51
 */
appControllers.controller('LoginCtrl', ['$scope',
    function LoginCtrl($scope) {

    }
]);
appDirectives.directive('tplMyData', function() {
	return {
		restrict: 'E',
		templateUrl: 'partials/directives/tpl.my.data.html'
	};
});
appFilters.filter('myFilter', function() {
	return function(input) {
		return input ? input : 'Nothing Fetch Yet!';
	}
})
/**
 * @author Christopher Biel <christopher.biel89@gmail.com>
 * @package ShipUp
 * @version 14.03.2015 16:48
 */
appServices.provider('LoginService', function () {
    var TOKEN_KEY = 'userToken',
        userToken = localStorage.getItem(TOKEN_KEY),
        errorState = 'app.error',
        logoutState = 'home';

    this.$get = function ($rootScope, $http, $q, $state) {

        /**
         * Low-level, private functions.
         */
        var setHeaders = function (token) {
            if (!token) {
                delete $http.defaults.headers.common['X-Token'];
                return;
            }
            $http.defaults.headers.common['X-Token'] = token.toString();
        };

        var setToken = function (token) {
            if (!token) {
                localStorage.removeItem('userToken');
            } else {
                localStorage.setItem('userToken', token);
            }
            setHeaders(token);
        };

        var getLoginData = function () {
            if (userToken) {
                setHeaders(userToken);
            } else {
                wrappedService.userRole = userRoles.public;
                wrappedService.isLogged = false;
                wrappedService.doneLoading = true;
            }
        };

        var managePermissions = function () {
            // Register routing function.
            $rootScope.$on('$stateChangeStart', function (event, to, toParams, from, fromParams) {

                /**
                 * $stateChangeStart is a synchronous check to the accessLevels property
                 * if it's not set, it will setup a pendingStateChange and will let
                 * the grandfather resolve do his job.
                 *
                 * In short:
                 * If accessLevels is still undefined, it let the user change the state.
                 * Grandfather.resolve will either let the user in or reject the promise later!
                 */
                if (wrappedService.userRole===null) {
                    wrappedService.doneLoading = false;
                    wrappedService.pendingStateChange = {
                        to: to,
                        toParams: toParams
                    };
                    return;
                }

                // if the state has undefined accessLevel, anyone can access it.
                // NOTE: if `wrappedService.userRole === undefined` means the service still doesn't know the user role,
                // we need to rely on grandfather resolve, so we let the stateChange success, for now.
                if (to.accessLevel===undefined || to.accessLevel.bitMask & wrappedService.userRole.bitMask) {
                    angular.noop(); // requested state can be transitioned to.
                } else {
                    event.preventDefault();
                    $rootScope.$emit('$statePermissionError');
                    $state.go(errorState, {error: 'unauthorized'}, {location: false, inherit: false});
                }
            });

            /**
             * Gets triggered when a resolve isn't fulfilled
             * NOTE: when the user doesn't have required permissions for a state, this event
             *       it's not triggered.
             *
             * In order to redirect to the desired state, the $http status code gets parsed.
             * If it's an HTTP code (ex: 403), could be prefixed with a string (ex: resolvename403),
             * to handle same status codes for different resolve(s).
             * This is defined inside $state.redirectMap.
             */
            $rootScope.$on('$stateChangeError', function (event, to, toParams, from, fromParams, error) {
                /**
                 * This is a very clever way to implement failure redirection.
                 * You can use the value of redirectMap, based on the value of the rejection
                 * So you can setup DIFFERENT redirections based on different promise errors.
                 */
                var errorObj, redirectObj;
                // in case the promise given to resolve function is an $http request
                // the error is a object containing the error and additional informations
                error = (typeof error==='object') ? error.status.toString() : error;
                // in case of a random 4xx/5xx status code from server, user gets loggedout
                // otherwise it *might* forever loop (look call diagram)
                if (/^[45]\d{2}$/.test(error)) {
                    wrappedService.logoutUser();
                }
                /**
                 * Generic redirect handling.
                 * If a state transition has been prevented and it's not one of the 2 above errors, means it's a
                 * custom error in your application.
                 *
                 * redirectMap should be defined in the $state(s) that can generate transition errors.
                 */
                if (angular.isDefined(to.redirectMap) && angular.isDefined(to.redirectMap[error])) {
                    if (typeof to.redirectMap[error]==='string') {
                        return $state.go(to.redirectMap[error], {error: error}, {location: false, inherit: false});
                    } else if (typeof to.redirectMap[error]==='object') {
                        redirectObj = to.redirectMap[error];
                        return $state.go(redirectObj.state, {error: redirectObj.prefix + error}, {
                            location: false,
                            inherit: false
                        });
                    }
                }
                return $state.go(errorState, {error: error}, {location: false, inherit: false});
            });
        };

        /**
         * High level, public methods
         */
        var wrappedService = {
            loginHandler: function (user, status, headers, config) {
                /**
                 * Custom logic to manually set userRole goes here
                 *
                 * Commented example shows an userObj coming with a 'completed'
                 * property defining if the user has completed his registration process,
                 * validating his/her email or not.
                 *
                 * EXAMPLE:
                 * if (user.hasValidatedEmail) {
         *   wrappedService.userRole = userRoles.registered;
         * } else {
         *   wrappedService.userRole = userRoles.invalidEmail;
         *   $state.go('app.nagscreen');
         * }
                 */
                    // setup token
                setToken(user.token);
                // update user
                angular.extend(wrappedService.user, user);
                // flag true on isLogged
                wrappedService.isLogged = true;
                // update userRole
                wrappedService.userRole = user.userRole;
                return user;
            },
            loginUser: function (httpPromise) {
                httpPromise.success(this.loginHandler);
            },
            logoutUser: function (httpPromise) {
                /**
                 * De-registers the userToken remotely
                 * then clears the loginService as it was on startup
                 */
                setToken(null);
                this.userRole = userRoles.public;
                this.user = {};
                this.isLogged = false;
                $state.go(logoutState);
            },
            resolvePendingState: function (httpPromise) {
                var checkUser = $q.defer(),
                    self = this,
                    pendingState = self.pendingStateChange;

                // When the $http is done, we register the http result into loginHandler, `data` parameter goes into loginService.loginHandler
                httpPromise.success(self.loginHandler);

                httpPromise.then(
                    function success(httpObj) {
                        self.doneLoading = true;
                        // duplicated logic from $stateChangeStart, slightly different, now we surely have the userRole informations.
                        if (pendingState.to.accessLevel===undefined || pendingState.to.accessLevel.bitMask & self.userRole.bitMask) {
                            checkUser.resolve();
                        } else {
                            checkUser.reject('unauthorized');
                        }
                    },
                    function reject(httpObj) {
                        checkUser.reject(httpObj.status.toString());
                    }
                );
                /**
                 * I setted up the state change inside the promises success/error,
                 * so i can safely assign pendingStateChange back to null.
                 */
                self.pendingStateChange = null;
                return checkUser.promise;
            },
            /**
             * Public properties
             */
            userRole: null,
            user: {},
            isLogged: null,
            pendingStateChange: null,
            doneLoading: null
        };

        getLoginData();
        managePermissions();

        return wrappedService;
    };
});
/**
 * @author Christopher Biel <christopher.biel89@gmail.com>
 * @package ShipUp
 * @version 08.03.2015 15:30
 */
appServices.factory('MyService', function($http, $q, Options) {
    return {
        read: function(id) {
            var deferred = $q.defer();

            $http.get(Options.baseUrl + '/read/' + id).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        create: function(data) {
            var deferred = $q.defer();

            $http.post(Options.baseUrl + '/create', {data: "my data"}).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        edit: function(data) {
            var deferred = $q.defer();

            $http.put(Options.baseUrl + '/edit', {data: "my data"}).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        delete: function(id) {
            var deferred = $q.defer();

            $http.delete(Options.baseUrl + '/delete/' + id).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        }
    };
});
/**
 * @author Christopher Biel <christopher.biel89@gmail.com>
 * @package ShipUp
 * @version 14.03.2015 16:15
 */
var baseUrlPath = '/session';

appServices.factory('SessionService', function ($http, $q, localStorageService, Options) {


    return {
        isActive: function (id) {
            var deferred = $q.defer();

            if (localStorageService.get(TOKEN_KEY)) {

            } else {

            }

            return deferred.promise;
        },


        read: function (id) {
            var deferred = $q.defer();

            $http.get(Options.baseUrl + baseUrlPath + '/read/' + id).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        create: function (data) {
            var deferred = $q.defer();

            $http.post(Options.baseUrl + baseUrlPath + '/create', {
                data: {
                    username: data.username,
                    password: data.password
                }
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        edit: function (data) {
            var deferred = $q.defer();

            $http.put(Options.baseUrl + baseUrlPath + '/edit', {data: "my data"}).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        delete: function (id) {
            var deferred = $q.defer();

            $http.delete(Options.baseUrl + baseUrlPath + '/delete/' + id).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        }
    };
});
/**
 * @author Christopher Biel <christopher.biel89@gmail.com>
 * @package ShipUp
 * @version 14.03.2015 15:03
 */
var baseUrlPath = '/user';

appServices.factory('UserService', function($http, $q, Options) {
    return {
        read: function(id) {
            var deferred = $q.defer();

            $http.get(Options.baseUrl + baseUrlPath + '/read/' + id).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        create: function(data) {
            var deferred = $q.defer();

            $http.post(Options.baseUrl + baseUrlPath + '/create', {data: "my data"}).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        edit: function(data) {
            var deferred = $q.defer();

            $http.put(Options.baseUrl + baseUrlPath + '/edit', {data: "my data"}).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        delete: function(id) {
            var deferred = $q.defer();

            $http.delete(Options.baseUrl + baseUrlPath + '/delete/' + id).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        }
    };
});